# Public Feedback Plugin

This project is a custom AtoM plugin. Learn more 
about AtoM at https://accesstomemory.org.

This plugin adds a button to the bottom of the right 
sidebar on content pages which opens up a Bootstrap 
modal that contains a form. The user can add in any 
feedback, which is then emailed to the site admins.

## Installation

Download and unzip the plugin to the `plugins` folder 
in your AtoM install.

Update `config/app.yml` with the following:

```!yaml
all:
  # ...

  mail:
    user_from: org@domain.com
  feedback:
    admin_email: editor@domain.com
  recaptcha:
    enabled: true
    site_key: [YOUR SITE KEY]
    secret_key: [YOUR SECRET KEY]
```

If you intend to use ReCAPTCHA then you will need to 
signup for a free API Key at https://www.google.com/recaptcha/admin.

If you do not want to use ReCAPTCHA then you can set `enabled: false`.

Finally enable the sfPublicFeedbackPlugin plugin in the 
web interface: `Admin > Plugins`.

## Support

Author: Jonathan Wagener (Plaas)

Email: jonathan@plaas.co