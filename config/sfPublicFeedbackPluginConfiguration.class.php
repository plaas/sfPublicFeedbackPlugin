<?php

class sfPublicFeedbackPluginConfiguration extends sfPluginConfiguration
{
  public static
    $summary = 'Allow users to provide feedback/suggestions on content.',
    $version = '1.0.0';

  public function contextLoadFactories(sfEvent $event)
  {
    $context = $event->getSubject();

    // Add some styles and scripts.
    $context->response->addStylesheet('/plugins/sfPublicFeedbackPlugin/modules/feedback/css/feedback.css');

    // Useful vendor functions for parsing strings and URLs
    $context->response->addJavaScript('/plugins/sfPublicFeedbackPlugin/modules/feedback/js/vendor/parse_url.js', 'last');
    $context->response->addJavaScript('/plugins/sfPublicFeedbackPlugin/modules/feedback/js/vendor/parse_str.js', 'last');

    // The feedback app JS.
    $context->response->addJavaScript('/plugins/sfPublicFeedbackPlugin/modules/feedback/js/feedback.js', 'last');

    // Add ReCAPTCHA API if enabled.
    if (sfConfig::get('app_recaptcha_enabled')) {
      $context->response->addJavaScript('https://www.google.com/recaptcha/api.js');
    }
  }

  /**
   * @see sfPluginConfiguration
   */
  public function initialize()
  {
    $this->dispatcher->connect('context.load_factories', array($this, 'contextLoadFactories'));

    $enabledModules = sfConfig::get('sf_enabled_modules');
    $enabledModules[] = 'sfCommentPlugin';
    sfConfig::set('sf_enabled_modules', $enabledModules);
  }
}
