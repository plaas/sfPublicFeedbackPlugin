(function($)
{
	$(document).ready(function()
	{
		var $parsedUrl = parse_url(window.location.href);
		var $arr = {};

		parse_str($parsedUrl.query, $arr);

		var $result = $arr;

		var activePath = window.location.pathname;

		if ($result.id)
		{
			activePath = activePath + '?id=' + $result.id;
		}
		
		if(window.location.pathname.match('browse') == null)
		{
			$('<div/>', {
			    id: 'feedback-plugin',
			}).appendTo($("body.informationobject, body.sfIsdiahPlugin, body.sfDcPlugin, body.sfModsPlugin, body.sfIsaarPlugin, body.sfRadPlugin, body.arDacsPlugin, body.sfIsadPlugin, body.sfPublicUploadPlugin").find("#context-menu"));
			
			$("#feedback-plugin").load("/feedback/button #sfPublicFeedbackPlugin", function() {
				grecaptcha.render("recaptcha1", {
					"sitekey": $("#recaptcha1").data("sitekey"),
					"theme": "light"
				});
			});
		}

		$(document).on("click", "button#feedback-send-feedback", function(e)
		{
			e.preventDefault();
			e.stopPropagation();

			if ($("#inputName").val() === "" || $("#inputEmail").val() === "" || $("#inputFeedback").val() === "")
			{
				alert("All of the fields are required.");
				return false;
			}

			var feedbackObj = {
				feedbackName: $("#inputName").val(),
				feedbackEmail: $("#inputEmail").val(),
				feedbackFeedback: $("#inputFeedback").val(),
				feedbackPath: activePath,
				feedbackRecaptchaResponse: grecaptcha.getResponse()
			};

			var submitResp = postFeedback(feedbackObj);

			if (submitResp.status === 'sent')
			{
				$('#feedbackModal').modal('hide');
				$("#feedback-form input, #feedback-form textarea").val("");
				grecaptcha.reset();

				alert("Thank you for your feedback. A member of the team will contact you shortly if necessary.");
			}
			else
			{
				grecaptcha.reset();
				alert("CAPTCHA failed, please try again.");
			}

			return false;
		});

		// Don't submit the form.
		$(document).on("submit", "form#feedback-form", function(e)
		{
			e.preventDefault();
			e.stopPropagation();
			return false;
		});
	});

	function postFeedback(obj)
	{
		var resp;

		$.ajax({
			type: "POST",   
		    url: "/feedback/submit",   
		    async: false,
		    data: obj,
		    success: function(result)
		    {
		    	resp = result;
		    },
		    error: function(error)
		    {
		    	resp = "{}";
		    }
		});
		
		return resp;
	}
})(jQuery);
