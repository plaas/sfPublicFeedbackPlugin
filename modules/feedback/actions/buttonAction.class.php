<?php

class feedbackbuttonAction extends sfAction
{
  public function execute($request)
  {
    $this->recaptchaEnabled = sfConfig::get('app_recaptcha_enabled');

    if ($this->recaptchaEnabled)
    {
        $this->recaptchaSiteKey = sfConfig::get('app_recaptcha_site_key');
        $this->recaptchaSecretKey = sfConfig::get('app_recaptcha_secret_key');
    }
  }
}
