<?php

class feedbacksubmitAction extends sfAction
{
  public function execute($request)
  {
    $continue = true;

    if (sfConfig::get('app_recaptcha_enabled'))
    {
        $recaptchaSecretKey = sfConfig::get('app_recaptcha_secret_key');
        $recaptchaResponse = $request->getPostParameter('feedbackRecaptchaResponse');

        $fields = array(
            'secret' => urlencode($recaptchaSecretKey),
            'response' => urlencode($recaptchaResponse),
        );

        foreach($fields as $key=>$value)
        {
            $fields_string .= $key .'='. $value .'&';
        }

        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close ($ch);

        if ($result->success)
        {
            $continue = true;
        }
    }

    if ($continue)
    {
        $name = $request->getPostParameter('feedbackName');
        $email = $request->getPostParameter('feedbackEmail');
        $feedback = $request->getPostParameter('feedbackFeedback');
        $path = str_replace(array('index.php/', 'qubit_dev.php/', 'index.php', 'qubit_dev.php'), '', $request->getPostParameter('feedbackPath'));
        $dateTime = time();

        // Extra the object ID from the path.
        $objectClass = '';
        $objectId = 0;

        $slug = str_replace('/', '', $path);

        $object = QubitObject::getBySlug($slug);

        $objectClass = $object->getClassName();
        $objectId = $object->getId();

        if (!$feedback || !$path)
        {
          return false;
        }

        $link = 'https://'. $_SERVER['HTTP_HOST'] .'/'. $slug;

        $moderateMail = new sfPHPView($this->getContext(), $this->getModuleName(), 'mail/feedbackEmail.html', null);
        $moderateMail->setAttribute('objectLink', $link, true);
        $moderateMail->setAttribute('objectTitle', $object->getTitle(), true);
        $moderateMail->setAttribute('feedbackName', $name, true);
        $moderateMail->setAttribute('feedbackEmail', $email, true);
        $moderateMail->setAttribute('feedbackFeedback', $feedback, true);
        $moderateMail->setAttribute('feedbackDate', $dateTime, true);

        $this->getMailer()->send(
          $this->getMailer()->compose()
            ->setFrom(array(sfConfig::get('app_mail_user_from') => (QubitSetting::getByName('siteTitle') ?: 'AtoM')))
            ->setTo(sfConfig::get('app_feedback_admin_email'))
            ->setSubject('New Feedback on '. $object->getTitle())
            ->addPart($moderateMail->render(), 'text/html')
        );

        $status = 'sent';
        $errors = 0;

        $data = array(
            'name' => $name,
            'email' => $email,
            'feedback' => $feedback,
            'path' => $path,
            'datetime' => date('d/m/Y', $dateTime),
            'objectClass' => $objectClass,
            'objectId' => $objectId,
            'title' => $object->getTitle(),
        );
    }
    else
    {
        $status = 'recaptcha-error';
        $errors = 1;

        $data = array();
    }

    $this->getResponse()->setContentType('application/json');
    echo json_encode(array(
      'status' => $status,
      'errors' => $errors,
      'data' => $data,
    ));
  }
}
