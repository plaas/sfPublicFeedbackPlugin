<?php decorate_with(false) ?>

<div style="display: none;">
	<div id="sfPublicFeedbackPlugin">
		<div class="feedback-button-wrapper">
			<a href="#feedbackModal" role="button" data-toggle="modal" class="feedback-button btn btn-danger">Have something to add?</a>
		</div>

		<!-- Modal -->
		<div id="feedbackModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="feedbackModal" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="feedbackModalLabel">Have something to add?</h3>
			</div>

			<div class="modal-body">
				<form class="form" id="feedback-form">
					<div class="control-group">
						<label class="control-label" for="inputName">Name*</label>
						
						<div class="controls">
							<input type="text" id="inputName" placeholder="Your Full Name" required="required">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="inputEmail">Email*</label>
						
						<div class="controls">
							<input type="email" id="inputEmail" placeholder="Your Email Address" required="required">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="inputFeedback">Feedback*</label>
						
						<div class="controls">
							<textarea id="inputFeedback" placeholder="Please type your feedback here..." required="required"></textarea>
						</div>
					</div>

					<?php if ($recaptchaSiteKey): ?>
					<div class="control-group">
						<div class="controls">
							<div id="recaptcha1" class="g-recaptcha" data-sitekey="<?php print $recaptchaSiteKey ?>"></div>
						</div>
					</div>
					<?php endif; ?>
				</form>
			</div>
			
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" id="feedback-send-feedback">Send Feedback</button>
			</div>
		</div>
	</div>
</div>