<?php decorate_with(false) ?>
<html>
	<head>

	</head>

	<body>
		<h3>
			New feedback on <a href="<?php print $objectLink ?>"><?php print $objectTitle ?></a>
		</h3>

		<br><hr>

		<p>
			<?php print date("d/m/Y", $feedbackDate) ?><br>
		 	<?php print $feedbackName ?> (<?php print $feedbackEmail ?>) has feedback on <a href="<?php print $objectLink ?>"><?php print $objectTitle ?></a>
		</p>

		<p>
			<?php print $feedbackFeedback ?>
		</p>

		<hr>
	</body>
</html>